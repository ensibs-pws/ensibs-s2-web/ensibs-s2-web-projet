# Academic Todo-list

## Get started
The next commands are pretty accurate if do not change the configuration.
1. Clone/download the repository
2. Install dependencies with `npm install`
3. Build CSS classes with `npx tailwindcss-cli@latest build ./assets/css/app.css -o ./assets/dist/app.css`
4. Launch Docker and run `docker-compose up`
5. Import SQL pattern `todolist.sql` into the database through `http://localhost:8080` with `root` : `example` credentials
6. Run `npm run serv` or `node index.js` to start server-side
7. Go to `http://localhost:3000` and enjoy ;)

## API documentation
All specified routes are after `/api/todos`.
### Get with GET verb
- `/api/todos` to get all todos.
- `/api/todos/:id` to get specific todo with ID.
### Create with POST verb
- `/api/todos` to create a todo. Automatic ID assignment. Request body require `libelle` argument.
### Create or replace with PUT verb
- `/api/todos/:id` to create a todo. Request body require `id`, `libelle`, `status` and `ordre` arguments. 201 if ID not already assigned, else 409. `overwrite` argument set to `true` allow you to replace a todo.
### Update with PATCH verb
- `/api/todos/:id` to modify an existing todo.
    - If `ordre` request body argument is set : set `ordre` attribute with ID value.
    - If `swap_id` request body argument is set with an existing ID : swap todos order.
    - If `status` request body argument is set with boolean : true or false set done or todo values.
### DELETE verb
- `/api/todos/:id` to delete an existing todo.
### Return value
SQL response is return on database modifications (contains inserted ID).