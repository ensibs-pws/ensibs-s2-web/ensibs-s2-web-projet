//////////////////////////////////
//        INITIALISATION        //
//////////////////////////////////

const bodyParser = require("body-parser");

const express = require("express");
const app = express();

app.set('view engine', 'ejs');
app.set('views', './views');
app.use(express.static(__dirname));

const db = require("./db.js");

//////////////////////////////////
//        REQUEST HANDLER       //
//////////////////////////////////

// Use Middlewares Express
app.use(bodyParser.json()); // Parse les requêtes avec content-type: application/json
app.use(bodyParser.urlencoded({ extended: true })); // Parse les requêtes avec content-type: application/x-www-form-urlencoded

// Display routes
app.get("/", (req, res) => {
  db.then(pool => 
    pool.query('SELECT * from taches ORDER BY ordre')
  ).then(todos => {
    res.render('index', {
      title: "Ma todolist",
      todolist: todos
    });
  }).catch(err => {
    res.statusCode = 500;
    res.render('error', {
      code: 500
    })
  });
});


// API routes
app.route("/api/todos")
.get((req, res) => {
  db.then(pool => 
    pool.query('SELECT * from taches ORDER BY ordre')
  ).then(todos => {
    res.statusCode = 200;
    res.send(todos);
  });
})
.post((req, res) => {
  db.then(pool => pool.query(`INSERT INTO taches (libelle) VALUES (?)`, req.body.libelle))
  .then((data) => {
    res.statusCode = 201;
    res.json(data);
  }).catch((err) => {
    res.sendStatus(500);
  });
})
.put((_, res) => res.sendStatus(405))
.patch((_, res) => res.sendStatus(405))
.delete((_, res) => res.sendStatus(405))

app.route("/api/todos/:id")
.get((req, res) => {
  db.then(pool => 
    pool.query('SELECT * from taches WHERE id = ?', req.params.id)
  ).then(todos => {
    if (todos[0]) {
      res.statusCode = 200;
      res.send(todos[0]);
    } else {
      res.sendStatus(404);
    }
  });
})
.post((_, res) => res.sendStatus(405))
.put((req, res) => {
  db.then(pool => 
    pool.query('SELECT * from taches WHERE id = ?', req.params.id)
  ).then(todos => {
    if (todos[0]) {
      if (req.body.overwrite === true) {
        query = `UPDATE taches SET libelle = ?, status = ?, ordre = ? WHERE id = ?`;
        db.then(pool => pool.query(query, [req.body.libelle, req.body.status, req.body.ordre, req.params.id]))
        .then((data) => {
          res.statusCode = 200;
          res.json(data);
        }).catch((err) => {
          res.sendStatus(500);
        });
      } else {
        res.sendStatus(409);
      }
    } else {
      db.then(pool => pool.query(`INSERT INTO taches (id, libelle, status, ordre) VALUES (?, ?, ?, ?)`, [req.params.id, req.body.libelle, req.body.status, req.params.id]))
      .then((data) => {
        res.statusCode = 201;
        res.json(data);
      }).catch((err) => {
        res.sendStatus(500);
      });
    }
  });
})
.patch((req, res) => {
  let params = [];
  if (req.body.ordre) {
    query = `UPDATE taches SET ordre = taches.id WHERE id = ?`;
    params = [req.params.id];
  } else if (req.body.swap_id) {
    query = `UPDATE taches AS rule1 JOIN taches AS rule2 ON (rule1.id = ? AND rule2.id = ? ) OR ( rule1.id = ? AND rule2.id = ? ) SET rule1.ordre = rule2.ordre, rule2.ordre = rule1.ordre`;
    params = [req.params.id, req.body.swap_id, req.body.swap_id, req.params.id];
  } else {
    if (req.body.status) {
      param = "FAIT";
      query = `UPDATE taches SET status = ? WHERE id = ?`;
      params = [param, req.params.id];
    } else {
      param = "A_FAIRE";
      query = `UPDATE taches SET status = ? WHERE id = ?`;
      params = [param, req.params.id];
    }
  }
  db.then(pool => pool.query(query, params))
  .then((data) => {
    if (data.changedRows) {
      res.statusCode = 200;
    } else if (!data.affectedRows) {
      res.statusCode = 204;
    } else {
      res.statusCode = 404;
    }
    res.json(data)
  }).catch((err) => {
    res.sendStatus(500);
  });
})
.delete((req, res) => {
  db.then(pool => pool.query(`DELETE FROM taches WHERE id = ?`, req.params.id))
  .then((data) => {
    if (data.affectedRows) {
      res.statusCode = 200;
    } else {
      res.statusCode = 404;
    }
    res.json(data);
  }).catch(() => {
    res.sendStatus(500);
  });
})


// Set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
